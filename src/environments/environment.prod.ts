export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBk4AwH2MmYZfE4KrkobhBojvSNJp_xEuo",
    authDomain: "fs1prod-6f777.firebaseapp.com",
    databaseURL: "https://fs1prod-6f777.firebaseio.com",
    projectId: "fs1prod-6f777",
    storageBucket: "fs1prod-6f777.appspot.com",
    messagingSenderId: "415029076731",
    appId: "1:415029076731:web:a86e9bcdfd622c3effc65e",
    measurementId: "G-3PPPTWFKJF"
  }
};
